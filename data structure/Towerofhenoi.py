#this is tower of henoi problem for 3 disks
#in this we move rings from sourec node (A) to destination node (c) and refrencenode(B) 
# 

def Towerofhenoi(n,A,C,B):
    if n==0:  #Base condition for stop recurssion
        return

    Towerofhenoi(n-1,A,B,C)              # it operates till all disk move from A to B
    print("move from " ,A, "to ", C)     #this statement executes when only one dick left

    Towerofhenoi(n-1,B,C,A)              #it operates till all disk move from B to C

Towerofhenoi(3,'A','C','B')     #function calling
