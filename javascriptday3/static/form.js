
$(document).ready(function(){
    var maxField = 10; 
    var wrapper = $(".input_fields_wrap"); 
    var addButton = $('.add_field_button'); 
    var id = 0;
    
    $(addButton).click(function(){
        var  x=1
        if(x < maxField){ 
        x++;
        $(wrapper).append(`<div class="item" id="cont-${id}"><input class="inpt" type="text" id="input${id}" value=""/><button type="button" id="remove_button">remove</button><button type="button" id="show" data-for="${id}">show</button></div>`); 
        id++;                     }   
    })

    function showTxt(id){
        var txt = $(`input${id}`).val();
        $(`#cont-${id}`).append(`<span>${txt}</span>`);
    }

    $(document).on("click","#remove_button", function(e){ 
        e.preventDefault(); $(this).parent('div').remove();
    })

    $(document).on("click","#show", function(e){ 
        var id = $(this).attr('data-for');
        var txt = $(`#input${id}`).val();
        $(`#cont-${id}`).append(`<p>${txt}</p>`);
    });

    $(document).on("click","#color",function(){
        $(`.item`).each(function(index) {
            var colorR = Math.floor(Math.random() * (225 - 0 + 1)) + 0;
            var colorG = Math.floor(Math.random() * (225 - 0 + 1)) + 0;
            var colorB = Math.floor(Math.random() * (225 - 0 + 1)) + 0;
            $(".inpt").css("background-color", "rgb(" + colorR + "," + colorG + "," + colorB + ")");
        });
    });
});

